--9.
drop function if exists graphical(integer);
create function graphical(integer) returns void as $$
declare
    h integer[];
    spaces text := '';
    tmp text;
begin
    select show_hierarchy($1) into h;
    for i in reverse array_length(h, 1) .. 1 loop
        tmp := spaces || h[i]::text;
        raise notice '%', tmp;
        spaces := spaces || ' ';
    end loop;
end;
$$ language plpgsql;

--select graphical(42);
