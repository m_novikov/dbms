--5.
drop function if exists show_hierarchy(integer);
create function show_hierarchy(integer) returns integer[] as $$
declare
    prev integer;
    l integer[];
    x integer;
begin
    l := array[$1];
    select boss_id into prev from data where id=$1;
    while (prev is not null) and (prev <> -1) loop
        l := l || array[prev];
        select boss_id into x from data where id=prev;
        prev := x;
    end loop;
    return l;
end;
$$ language plpgsql;

--select * from show_hierarchy(42);
