--6.
drop function if exists depth(integer);
create function depth(integer) returns bigint as $$
    select count(*) from show_depart($1);
$$ language sql;

--select depth(91);
