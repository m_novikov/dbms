--3.
drop function if exists show_depart(integer);
create function show_depart(integer) returns setof data as $$
    select * from data where boss_id=$1
$$ language sql;

--select show_depart(91);
