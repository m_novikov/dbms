--10.
drop function if exists route(integer, integer);
create function route(integer, integer) returns integer[] as $$
declare
    lst1 integer[];
    lst2 integer[];
    rt integer[];
    i1 integer;
    i2 integer;
    parent integer;
begin
    select show_hierarchy($1) into lst1;
    select show_hierarchy($2) into lst2;
    select array_length(lst1, 1) into i1;
    select array_length(lst2, 1) into i2;
    parent := -1;
    while (lst1[i1] is not null) and (lst2[i2] is not null) loop
        if lst1[i1] <> lst2[i2] then
            exit;
        end if;
        parent := lst1[i1];
        i1 := i1 - 1;
        i2 := i2 - 1;
    end loop;
    for i in 1 .. i1 loop
        rt := rt || lst1[i];
    end loop;
    rt := rt || parent;
    for i in reverse  i2 .. 1 loop
        rt := rt || lst2[i];
    end loop;
    return rt;
end;
$$ language plpgsql;

--select * from show_hierarchy(42);
--select * from show_hierarchy(78);
--select route(42, 78);
