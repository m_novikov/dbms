--2.
drop function if exists move_person(integer, integer);
create function move_person(integer, integer) returns void as $$
    update data set boss_id=$2 where id=$1
$$ language sql;

--select move_person(101, 1);
