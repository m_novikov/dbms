--8.
drop function if exists rank(integer);
create function rank(integer) returns integer as $$
declare
    x integer[];
begin
    select * into x from show_hierarchy($1);
    return coalesce(array_length(x, 1), 0);
end;
$$ language plpgsql;

--select rank(42);
