--7.
drop language if exists plpython2u cascade;
create language plpython2u;
drop function if exists consistency();
create function consistency() returns text as $$
    tbl = plpy.execute('select * from data')
    d = {}
    boss_id = 1
    boss_found = False
    for person in tbl:
        id = person['id']
        boss = person['boss_id']
        if id not in d:
            d[id] = []
        if not boss:
            return 'Bad table'
        if boss == -1:
            if boss_found:
                return 'Bad table'
            boss_found = True
            boss_id = id
            continue
        if boss in d:
            d[boss].append(id)
        else:
            d[boss] = [id]
    if not boss_found:
        return 'Bad table'
    marks = { key:0 for key in d.keys() }
    people = [boss_id]
    while len(people) > 0:
        person = people[0];
        if marks[person] == 1:
            return 'Bad table'
        marks[person] = 1;
        people = people[1:] + d[person]
    if reduce(lambda x, y: x + y, marks.values()) == len(marks.values()):
        return 'Good table'
    else:
        return 'Bad table'
$$ language plpython2u;

--select consistency();
