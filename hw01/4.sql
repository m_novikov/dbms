--4.
drop function if exists show_leaves();
create function show_leaves() returns setof data as $$
    select * from data where id not in
        (select distinct boss_id from data where boss_id is not null)
$$ language sql;

--select show_leaves();
