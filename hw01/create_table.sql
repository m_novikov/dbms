drop table if exists data cascade;
create table data(
    id serial primary key,
    boss_id integer,
    name varchar(80)
);
