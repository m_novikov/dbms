-- 1.
drop function if exists add_person(varchar(80));
create function add_person(varchar(80)) returns void as $$
    insert into data (id, name) values(default, $1)
$$ language sql;

--select add_person('Maxim');
